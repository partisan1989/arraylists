package task2;


import java.time.DayOfWeek;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Article {
    String title;
    Calendar date;
    ArrayList<String> tag = new ArrayList<>();    //list of tags

    public Article(String title, int dateIncrement, String tag) {
        this.title = title;
        this.date = new GregorianCalendar();    //setting today date
        this.date.add(Calendar.DAY_OF_YEAR, dateIncrement);  //setting today date + dateIncrement
        this.tag.add(tag);
    }

    @Override
    public String toString() {
        return "Article {" +
                "title='" + title + '\'' +
                ", date=" + date.get(Calendar.YEAR) + "/" + date.get(Calendar.MONTH) + "/" + date.get(Calendar.DAY_OF_MONTH) +
                ", tags='" + tag.get(0) +"," + tag.get(1) +"," + tag.get(2)+ '\'' +
                '}';
    }

    @Override    //making it ignore tags and compare only title & date
    public boolean equals(Object o) {
        //if (this == o) return true;
        //if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (title.equals(article.title) && date.equals(article.date)) return true;
        else return false;
        //if (!title.equals(article.title)) return false;
        //return date.equals(article.date);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}
