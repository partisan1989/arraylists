package task2;

import java.util.ArrayList;

public class Main {

    static final int n = 3;
    static ArrayList<Article> listOne = new ArrayList<>(n);   //list of Articles #1
    static ArrayList<Article> listTwo = new ArrayList<>(n);   //list of Articles #2

    static ArrayList<Article> similarArticles = new ArrayList<>();   //list of possible similar articles

    public static void main(String[] args) {

        System.out.println("listOne:");

        for (int i = 0; i < n; i++) {
            Article article = new Article("title" + i, i, "tag" + i);  //create article

            article.tag.add("tag" + (i + 1));   //add tags
            article.tag.add("tag" + (i + 2));

            listOne.add(article);   //add article to list #1

            System.out.println(listOne.get(i));    //output article
        }

        System.out.println();
        System.out.println("listTwo:");

        for (int i = 0; i < n; i++) {
            Article article = new Article("title" + i, i, "tag" + i);   //create article

            article.tag.add("tag" + (i + 1));   //add tags
            article.tag.add("tag" + (i + 2));

            listTwo.add(article);    //add article to list #2

            System.out.println(listTwo.get(i));    //output article
        }

        similarArticles = getSimilarArticles(listOne, listTwo);   // get list of similar articles (the same title&date)

        System.out.println();
        System.out.println("Similar Articles count: " + similarArticles.size());  //output of similar articles number

        for (int i=0; i<similarArticles.size();i++) {    //output similar articles list
            System.out.println(similarArticles.get(i));
        }



    }

    public static ArrayList<Article> getSimilarArticles(ArrayList<Article> list1, ArrayList<Article> list2) {
    //method to find similar articles

        ArrayList<Article> similarArticles = new ArrayList<>();   //the list of similar articles

        for (int i = 0; i < list1.size(); i++) {     //find which articles from list #1 are present in list #2
            if (list2.contains(list1.get(i)) && !similarArticles.contains(list1.get(i))) {
                similarArticles.add(list1.get(i));
            }

        }

        //just as alternative variant:
        for (int i = 0; i < list2.size(); i++) {     //find which articles from list #2 are present in list #1
            if (list1.contains(list2.get(i)) && !similarArticles.contains(list2.get(i))) {
                similarArticles.add(list2.get(i));
            }

        }


        // finds the articles with the same title and date (ignoring tags)

//        for (int i=0; i<list1.size();i++)
//            for (int j=0; j<list2.size(); j++)
//            {
//                if (list2.get(j).title.equals(list1.get(i).title) && list2.get(j).date==list1.get(i).date) {
//                 if (!similarArticles.contains(list2.get(j)))
//                    {
//                        similarArticles.add(list2.get(j));
//                    }
//                }
//            }

        return similarArticles;
    }
}
